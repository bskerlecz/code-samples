<?php

namespace backend\controllers;

use common\components\Logger;
use common\models\User;
use yii\rest\Controller;
use yii\web\NotFoundHttpException;
use yii\web\ServerErrorHttpException;

/**
 * Registration controller
 *
 * Class RegistrationController
 * @package backend\controllers
 */
class RegistrationController extends Controller {

    public $collectionOptions   = ['GET', 'POST', 'OPTIONS'];
    public $resourceOptions     = ['GET', 'PUT', 'DELETE', 'OPTIONS'];

    public function actionOptions($id = null) {
        if (\Yii::$app->getRequest()->getMethod() !== 'OPTIONS') {
            \Yii::$app->getResponse()->setStatusCode(405);
        }
        $options = $id === null ? $this->collectionOptions : $this->resourceOptions;
        \Yii::$app->getResponse()->getHeaders()->set('Allow', implode(', ', $options));
    }

    /**
     * Register new user
     *
     * @return User
     * @throws ServerErrorHttpException
     * @throws \yii\base\InvalidConfigException
     */
    public function actionIndex() {
        $user = new User(['scenario' => User::SCENARIO_REGISTER]);
        $user->load(\Yii::$app->request->getBodyParams(), '');
        $user->loadAttributeDefaults();
        $user->setPasswordHash($user->password);
        $user->setActivationHash();

        if($user->save()) {
            $registrationConfig = \Yii::$app->params['registration'];

            try {
                $sendResult = \Yii::$app->mailer
                    ->compose('accountActivation', ['user' => $user])
                    ->setFrom($registrationConfig['from'])
                    ->setTo($user->email)
                    ->setSubject(\Yii::t('app', 'TakeNote registration'))
                    ->send();

                if(!$sendResult) {
                    throw new \Exception(\Yii::t('app', "Could not send email."));
                }
            } catch (\Exception $e) {
                Logger::error($e);
                $user->delete(); // delete the user in case if no email was sent so he/she will be able to sign up again
                throw new ServerErrorHttpException(\Yii::t('app', 'Failed to send activation email.'), 0, $e);
            }
        } elseif (!$user->hasErrors()) {
            $user->delete();
            throw new ServerErrorHttpException(\Yii::t('app','Failed to update the object for unknown reason.'));
        }

        return $user;
    }

    /**
     * Activate user by activation-hash
     *
     * @param $id
     * @return mixed
     * @throws NotFoundHttpException
     * @throws ServerErrorHttpException
     */
    public function actionActivate($id) {
        $user = User::findByActivationHash($id);
        if($user == null) {
            throw new NotFoundHttpException(\Yii::t('app', 'User not found with given hash'));
        }

        switch ($user->status)
        {
            case User::STATUS_PENDING:
                $user->status = User::STATUS_ACTIVE;
                if($user->update(true, ['status']) == FALSE) {
                    if($user->hasErrors()) {
                        return $user;
                    } else {
                        throw new ServerErrorHttpException(\Yii::t('app','Failed to update the object for unknown reason.'));
                    }
                }
                break;
            case User::STATUS_DELETED:
                Logger::warning(sprintf("Activation requested for deleted user [%s]", $user->_id));
                throw new NotFoundHttpException(\Yii::t('app', "Activation requested for invalid user."));
                break;
            case User::STATUS_ACTIVE:
                throw new NotFoundHttpException(\Yii::t('app', "User already activated."));
                break;
        }

        \Yii::$app->response->statusCode = 204;
        return [];
    }

    /**
     * Set new password for user with given password_reset_token
     *
     * @param $id
     * @return User|null
     * @throws NotFoundHttpException
     * @throws ServerErrorHttpException
     */
    public function actionPasswordReset($id) {
        \Yii::trace("password-reset, user-id: {$id}");

        $user = User::findByPasswordResetToken($id);
        if ($user == null || !$user->isActive()) {
            throw new NotFoundHttpException(\Yii::t('app', 'Token not found.'));
        }

        $newPassword = \Yii::$app->request->getBodyParam('password');
        $user->scenario = User::SCENARIO_CHANGE_PASSWORD;
        $user->password = $newPassword;
        $user->setPasswordHash($user->password);
        $user->removePasswordResetToken();

        if ($user->update()) {
            Logger::trace(sprintf("Password changed for user [%s]", $user->_id));
            \Yii::$app->response->statusCode = 204;
            return [];
        } elseif (!$user->hasErrors()) {
            throw new ServerErrorHttpException(\Yii::t('app','Failed to update the object for unknown reason.'));
        }

        return $user;
    }

    /**
     * Request a new password_reset_token for an user
     *
     * @param $id email
     * @return array|User
     * @throws NotFoundHttpException
     * @throws ServerErrorHttpException
     * @throws \Exception
     */
    public function actionPasswordResetToken($id) {
        /** @var User $user */
        $user = User::findByEmail($id);
        if ($user == null) {
            throw new NotFoundHttpException(\Yii::t('app', 'User not found.'));
        }

        $user->generatePasswordResetToken();

        if ($user->update(true, ['password_reset_token'])) {
            $userConfig = \Yii::$app->params['user'];

            try {
                $sendResult = \Yii::$app->mailer
                    ->compose('passwordReset', ['user' => $user])
                    ->setFrom($userConfig['from'])
                    ->setTo($user->email)
                    ->setSubject(\Yii::t('app', 'TakeNote password reset'))
                    ->send();

                if (!$sendResult) {
                    throw new \Exception(\Yii::t('app', 'Could not send email.'));
                }
            } catch (\Exception $e) {
                $user->removePasswordResetToken();
                $user->update(true, ['password_reset_token']);

                throw new ServerErrorHttpException(\Yii::t('app', "Failed to send password reset email."), 0, $e);
            }

            \Yii::$app->response->statusCode = 204;
            return [];

        } elseif (!$user->hasErrors()) {
            throw new ServerErrorHttpException(\Yii::t('app','Failed to update the object for unknown reason.'));
        }

        return $user;
    }
}