<?php

namespace backend\components;

use Yii;
use yii\base\ActionFilter;
use yii\web\ForbiddenHttpException;

/**
 * Access control filter by IP address
 *
 * Class HttpClientIpFilter
 * @package backend\versions\common\components
 */
class HttpClientIpFilter extends ActionFilter
{

    /** @var array Allowed client ip-s */
    public $allowedIpList = [];

    /** @var array Disallowed client ip-s */
    public $deniedIpList = [];

    public function init()
    {
        parent::init();
    }

    public function beforeAction($action)
    {
        $denied = false;
        $ip = \Yii::$app->request->getUserIP();
        $token = self::className() . "({$ip})";
        \Yii::info(
            $token .
            " allowed-ip-list: " . json_encode($this->allowedIpList) .
            ", denied-ip-list: " . json_encode($this->deniedIpList)
        );

        if(is_array($this->allowedIpList) && count($this->allowedIpList)) {
            // If allowedIpList given allow for given ips or regexps and deny all others
            $denied = true;
            foreach ($this->allowedIpList as $allowedIp) {
                $pattern = '/'.$allowedIp.'/';
                $subject = $ip;
                if(preg_match($pattern, $subject)) {
                    $denied = false;
                    break;
                }
            }
        }

        if (is_array($this->deniedIpList) && count($this->deniedIpList)) {
            // If deniedIpList given deny only for given ips or regexps
            $denied = false;
            foreach ($this->deniedIpList as $deniedIp) {
                $pattern = '/'.$deniedIp.'/';
                $subject = $ip;
                if(preg_match($pattern, $subject)) {
                    $denied = true;
                    break;
                }
            }
        }

        \Yii::info($token . " " . ($denied ? 'denied' : 'allowed'));
        if ($denied) {
            throw new ForbiddenHttpException(Yii::t('yii', 'You are not allowed to perform this action.'));
        }

        return parent::beforeAction($action);
    }
}