<?php

namespace backend\components;

use PayPal\Api\Payment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\Webhook;
use PayPal\Api\WebhookEvent;
use PayPal\Api\WebhookEventType;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Rest\ApiContext;
use yii\base\Component;

/**
 * PayPal Service integration as a Yii2 component
 *
 * Class PaypalService
 * @package backend\components
 */
class PaypalService
{
    const PAYPAL_MAX_WEBHOOK_LIMIT = 10;

    const WEBHOOK_PAYMENT_NOTIFICATION = '';

    public $name;
    public $clientId;
    public $secret;
    public $mode;
    public $logFile;
    public $callback;

    /** @var  OAuthTokenCredential */
    private $credential;

    /** @var  ApiContext */
    private $apiContext;

    public function init()
    {
        parent::init();

        $logLevel = 'FINE';
        $this->credential = new OAuthTokenCredential($this->clientId, $this->secret);
        $this->apiContext = new ApiContext($this->credential);
        $this->logFile = \Yii::getAlias($this->logFile);

        if (defined('YII_DEBUG') && YII_DEBUG) {
            $logLevel = 'DEBUG';
        }

        $this->apiContext->setConfig([
            'mode' => $this->mode,
            'log.LogEnabled' => true,
            'log.FileName' => $this->logFile,
            'log.LogLevel' => $logLevel,
            // 'cache.enabled' => true,
            // 'http.CURLOPT_CONNECTTIMEOUT' => 30
        ]);

        \Yii::info("Initializing {$this->className()}, mode: {$this->mode}, logging to: {$this->logFile}");
    }

    /**
     * Return registered webhooks for this application
     *
     * @return \PayPal\Api\WebhookList
     */
    public function getRegisteredWebhooks() {
        return Webhook::getAll($this->apiContext);
    }

    /**
     * Registers a callback url for all webhook events
     *
     * @param $url
     * @return Webhook
     */
    public function addWebhookForUrlAllEventTypes($url) {
        $allEvents = WebhookEventType::availableEventTypes($this->apiContext);
        $webhook = new Webhook();
        $webhook->setUrl($url);
        $webhook->setEventTypes($allEvents->getEventTypes());

        return $webhook->create($this->apiContext);
    }

    /**
     * Remove a registered webhook
     *
     * @param Webhook $webhook
     * @return bool
     */
    public function removeWebhook(Webhook $webhook) {
        return $webhook->delete($this->apiContext);
    }

    /**
     * Get registered webhooks for a given url
     *
     * @param $url
     * @return \PayPal\Api\Webhook[]
     */
    public function getWebhooksForUrl($url) {
        /** @var Webhook[] $webhooksForUrl */
        $webhooksForUrl = [];
        $webhooks = $this->getRegisteredWebhooks()->getWebhooks();
        if (count($webhooks) > 0) {
            foreach ($webhooks as $webhook) {
                if ($webhook->getUrl() == $url) {
                    $webhooksForUrl[] = $webhook;
                }
            }
        }

        return $webhooksForUrl;
    }

    /**
     * Get application's webhook url
     *
     * @return mixed
     */
    public function getWebhookUrl() {
        return $this->callback['webhookEndpoint'];
    }

    /**
     * Execute a payment
     *
     * @param $paymentId
     * @param $payerId
     * @return mixed
     */
    public function executePayment($paymentId, $payerId) {
        \Yii::trace("payment-execute, payment-id: {$paymentId}, payer-id: {$payerId}");
        $payment = Payment::get($paymentId, $this->apiContext);
        $execution = new PaymentExecution();
        $execution->setPayerId($payerId);

        return $payment->execute($execution, $this->apiContext);
    }

    /**
     * Get payment information
     *
     * @param $paymentId
     * @return mixed
     */
    public function getPayment($paymentId) {
        \YIi::trace("get-payment, payment-id: {$paymentId}");
        return Payment::get($paymentId, $this->apiContext);
    }

    /**
     * Create a payment
     *
     * @param Payment $payment
     * @return mixed
     */
    public function createPayment(Payment $payment) {
        \Yii::trace("payment-create, data: {$payment->toJSON()}");
        return $payment->create($this->apiContext);
    }

    /**
     * Parses a raw webhook http request into a WebhookEvent
     *
     * @param $requestBodyStr
     * @return WebhookEvent
     * @throws \PayPal\Exception\PayPalConnectionException
     */
    public function parseNotificationEvent($requestBodyStr) {
        $event = WebhookEvent::validateAndGetReceivedEvent($requestBodyStr, $this->apiContext);

        return $event;
    }

    /**
     * Returns the payment approval success URL for a given payment
     *
     * @param $paymentId
     * @return string
     */
    public function getApprovalSuccessUrl($paymentId) {
        $successUrl = $this->callback['approvalSuccess'];
        return strtr($successUrl, ['{paymentId}' => $paymentId]);
    }

    /**
     * Returns the payment approval cancel URL for a given payment
     *
     * @param $paymentId
     * @return string
     */
    public function getApprovalCancelUrl($paymentId) {
        $cancelUrl = $this->callback['approvalCancel'];
        return strtr($cancelUrl, ['{paymentId}' => $paymentId]);
    }


    public static function parsePaypalApprovalLink($link) {
        $parsed = parse_url($link);
        $validLink = true;
        $token = null;

        if (!empty($parsed) && isset($parsed['query'])) {
            parse_str($parsed['query'], $query);

            if (!empty($query) && isset($query['token'])) {
                $token = $query['token'];
            } else {
                $validLink = false;
            }
        } else {
            $validLink = false;
        }

        if (!$validLink) {
            return false;
        } else {
            return [
                'token' => $token
            ];
        }
    }
}