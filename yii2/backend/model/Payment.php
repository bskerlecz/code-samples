<?php

namespace backend\models;


use backend\components\PaypalService;
use PayPal\Core\PayPalConfigManager;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;
use yii\mongodb\ActiveRecord;
use yii\mongodb\validators\MongoIdValidator;
use yii\web\ServerErrorHttpException;

/**
 * Model to store payments from 3rd-party providers
 *
 * @package common\models
 *
 * @property $_id
 * @property $paypal_payer_id
 * @property $paypal_payment_id
 * @property $paypal_token
 * @property $paypal_approval_link
 * @property $provider
 * @property $status
 * @property $created_at
 * @property $update_at
 *
 */
class Payment extends ActiveRecord
{
    // Status constants
    const STATUS_EMPTY = 'empty';
    const STATUS_CREATED = 'created';
    const STATUS_APPROVED = 'approved';
    const STATUS_CANCELLED = 'cancelled';
    const STATUS_DONE = 'done';

    // Provider constants
    const PROVIDER_PAYPAL = 'paypal';

    /**
     * @inheritdoc
     * @return mixed
     */
    public function rules()
    {
        return ArrayHelper::merge(
            parent::rules(),
            [
                [['_id'], MongoIdValidator::className(), 'forceFormat' => 'object'],

                [['provider'], 'required'],
                ['provider', 'in', 'range' => [ self::PROVIDER_PAYPAL]],

                ['status', 'in', 'range' => [self::STATUS_EMPTY, self::STATUS_CREATED, self::STATUS_APPROVED, self::STATUS_CANCELLED, self::STATUS_DONE]],
                ['status', 'default', 'value' => self::STATUS_EMPTY]
            ]
        );
    }

    /**
     * @inheritdoc
     * @return mixed
     */
    public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            [
                TimestampBehavior::className()
            ]
        );
    }

    /**
     * @inheritdoc
     * @return array
     */
    public function attributes()
    {
        return [
            '_id',
            'paypal_payer_id',
            'paypal_payment_id',
            'paypal_token',
            'paypal_approval_link',
            'provider',
            'status',
            'created_at',
            'updated_at'
        ];
    }

    /**
     * @inheritdoc
     * @return string
     */
    public static function collectionName()
    {
        return 'client_payment';
    }

    /**
     * Get the corresponding order for this payment
     *
     * @return \yii\db\ActiveQueryInterface
     */
    public function getOrder() {
        return $this->hasOne(Order::className(), ['payment_id' => '_id']);
    }

    /**
     * @param $token
     * @return null|Payment
     */
    public static function findByPaypalToken($token) {
        return static::findOne(['paypal_token' => $token]);
    }

    /**
     * Update current payment with data received from PayPal
     *
     * @param \PayPal\Api\Payment $paypalPayment
     */
    public function updateWithPaypalData(\PayPal\Api\Payment $paypalPayment) {
        $approval_link = $paypalPayment->getApprovalLink();

        $data = PaypalService::parsePaypalApprovalLink($approval_link);
        if ($data === false) {
            \Yii::error("Cannot parse 'approvalLink', invalid format: " . VarDumper::dumpAsString($approval_link));
            throw new ServerErrorHttpException(\Yii::t('app', 'Service returned invalid approval url.'));
        }

        // Store the parsed data in this model
        $token = $data['token'];
        $this->paypal_payment_id = $paypalPayment->id;
        $this->paypal_token = $token;
        $this->paypal_approval_link = $approval_link;
        $this->status = static::STATUS_CREATED;

        // Save payment
        if (!$this->save()) {
            throw new ServerErrorHttpException(\Yii::t('app', 'Cannot create Payment.'));
        }
    }
}