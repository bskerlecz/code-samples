<?php

namespace console\models;

use yii\helpers\ArrayHelper;
use yii\mongodb\ActiveRecord;

/**
 * Dictionary model
 *
 * @package console\models
 *
 * @property $type
 * @property $name
 * @property $identifier
 * @property $_id
 *
 */
class Dictionary extends ActiveRecord
{

    const TYPE_SERVICE = 'service';
    const TYPE_TURNAROUND_TIME = 'turnaround_time';
    const TYPE_SPEAKERS = 'speakers';
    const TYPE_ADDITIONAL_SERVICES = 'additional_services';

    public static function dictionaryTypes() {
        return [
            static::TYPE_ADDITIONAL_SERVICES,
            static::TYPE_SPEAKERS,
            static::TYPE_SERVICE,
            static::TYPE_TURNAROUND_TIME
        ];
    }

    public static function collectionName()
    {
        return 'service_dictionary';
    }

    public function rules()
    {
        return ArrayHelper::merge(
            parent::rules(),
            [
                [['type', 'name', 'identifier'], 'required'],
                [['name', 'identifier'], 'string'],

                ['identifier', 'unique'],
                ['type', 'in', 'range' => [
                    static::TYPE_SERVICE,
                    static::TYPE_TURNAROUND_TIME,
                    static::TYPE_SPEAKERS,
                    static::TYPE_ADDITIONAL_SERVICES
                ]],

            ]
        );
    }

    public static function findByIdentifier($identifier) {
        return static::find()->where(['identifier' => $identifier])->one();
    }

    public static function findByType($type) {
        return static::find()->where(['type' => $type])->all();
    }

    public function attributes()
    {
        return [
            '_id',
            'identifier',
            'name',
            'type',
        ];
    }
}