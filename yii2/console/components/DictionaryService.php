<?php

namespace console\components;


use console\models\Dictionary;
use yii\base\Component;

/**
 * Simple Yii2 component for handling the domain specific dictionary
 *
 * Class DictionaryService
 * @package console\components
 */
class DictionaryService extends Component
{
    private $dictionaries;

    public function init()
    {
        parent::init();

        $this->dictionaries = [];
    }

    public function hasWordInDictionary($word, $dictionaryType) {
        $dictionary = $this->getDictionary($dictionaryType);
        foreach ($dictionary as $item) {
            /** @var Dictionary $item */
            if ($word == $item->identifier) {
                return true;
            }
        }

        return false;
    }

    public function getDictionaryConstants($dictionaryType) {
        $constants = [];
        $dictionary = $this->getDictionary($dictionaryType);

        foreach ($dictionary as $item) {
            /** @var Dictionary $item */
            $constants[$item->identifier] = $item->name;
        }

        return $constants;
    }

    /**
     * @param $dictionaryType
     * @return Dictionary[]|null
     */
    public function getDictionary($dictionaryType) {
        if (in_array($dictionaryType, Dictionary::dictionaryTypes())) {
            if (!isset($this->dictionaries[$dictionaryType])) {
                $this->dictionaries[$dictionaryType] = Dictionary::findByType($dictionaryType);
            }

            return $this->dictionaries[$dictionaryType];
        }

        return null;
    }
}