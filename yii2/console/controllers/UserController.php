<?php
namespace console\controllers;

use common\models\User;
use console\components\ConsoleController;
use yii\helpers\VarDumper;

/**
 * Basic CLI to manage users
 *
 * Class UserController
 * @package console\controllers
 */
class UserController extends ConsoleController
{

    /**
     * Add a new user with validation
     *
     * @param array $params
     * @return mixed
     */
    public function actionAdd(array $params) {
        $parsedParams = $this->parseTaskParameters($params);
        $this->title("Add new user");
        $this->info("params: " . VarDumper::dumpAsString($parsedParams));

        $user = new User(['scenario' => User::SCENARIO_CREATE]);
        $user->setAttributes($parsedParams);
        if ($user->validate()) {
            $user->status = User::STATUS_ACTIVE;
            $user->save();
            $user->refresh();
            $this->success("User created.");
            $this->info(VarDumper::dumpAsString($user->toArray()));
            return self::EXIT_CODE_NORMAL;
        } else if ($user->hasErrors()) {
            $this->error("Validation failed.");
            $this->info(VarDumper::dumpAsString($user->errors));
            return self::EXIT_CODE_ERROR;
        } else {
            $this->error("Cannot create user, unknown error.");
            return self::EXIT_CODE_ERROR;
        }
    }

    /**
     * Delete user
     *
     * @param $userId
     * @return mixed
     */
    public function actionDelete($userId) {
        /** @var User $user */
        $this->title("Delete user: {$userId}");
        $user = User::findOne(['_id' => $userId]);
        if (!$user) {
            $this->warning("User not found.");
            return self::EXIT_CODE_ERROR;
        }

        if ($user->delete()) {
            $this->success("User deleted.");
            return self::EXIT_CODE_NORMAL;
        } else {
            $this->error("Cannot delete user, unknown error.");
            return self::EXIT_CODE_ERROR;
        }
    }

    /**
     * Changes given user's role to a new one
     *
     * @param $userId
     * @param $newRole
     * @return mixed
     */
    public function actionChangeRole($userId, $newRole) {
        /** @var User $user */
        $this->title("Changing user({$userId}) role to {$newRole}");

        $user = User::findOne(['_id' => $userId]);
        if (!$user) {
            $this->warning("User not found.");
            return self::EXIT_CODE_ERROR;
        }

        $user->setAttributes([
            'role' => $newRole
        ]);

        if ($user->validate()) {
            $user->save();
            $user->refresh();
            $this->success("User saved.");
            $this->info(VarDumper::dumpAsString($user->toArray()));
            return self::EXIT_CODE_NORMAL;
        } else if ($user->hasErrors()) {
            $this->error("Validation failed.");
            $this->info(VarDumper::dumpAsString($user->errors));
            return self::EXIT_CODE_ERROR;
        } else {
            $this->error("Cannot create user, unknown error.");
            return self::EXIT_CODE_ERROR;
        }
    }

    /**
     * Parse CLI parameter format: user=foo name=bar
     *
     * @param array $params
     * @return array|int
     */
    private function parseTaskParameters(array $params) {
        // Parse passed parameters
        $parsedParams = [];
        if(is_array($params)) {
            $i=0;
            foreach ($params as $paramStr) {
                $assignment = explode("=", $paramStr);
                if(count($assignment)!=2) {
                    $this->stderr("Invalid parameter format at position: {$i}, value: '$paramStr'. (Example: a=123)" . PHP_EOL);
                    return 1;
                }

                list($paramName, $paramValue) = $assignment;
                $parsedParams[$paramName] = $paramValue;
                $i++;
            }
        }

        return $parsedParams;
    }
}