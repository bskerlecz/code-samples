<?php

namespace console\controllers;

use common\queue\models\QueueItem;
use common\queue\QueueManager;
use console\components\ConsoleController;
use yii\helpers\VarDumper;

/**
 * Manage MongoDb based job queue
 *
 * Class QueueController
 * @package console\controllers
 */
class QueueController extends ConsoleController
{
    const WORKER_SLEEP_TIME = 5; // waiting 5 sec between executing a new job

    /** @var QueueManager */
    private $queue;

    /**
     * Load Queue component before every action
     *
     * @param $action
     * @return mixed
     */
    public function beforeAction($action)
    {
        $this->queue = \Yii::$app->queue;
        return parent::beforeAction($action);
    }

    /**
     * PCNTL shutdown signal handler
     */
    public function onShutdown() {
        $this->info("Shutdown called, exiting.");
    }

    /**
     * General PCNTL signal handler
     * @param $signo
     */
    public function onSignal($signo) {
        $this->info("Received PCNTL signal {$signo}");
        die();
    }

    /**
     * Run a Queue worker instance
     * - This method is fetching jobs at a given intervals and executes them
     * - A Queue worker runs untils its get interrupted by a PCNTL signals
     *
     * @param bool $priorityLevel
     */
    public function actionRunWorker($priorityLevel = false) {
        set_time_limit(0);
        declare(ticks = 1);

        // Register PCNTL signal handlers
        register_shutdown_function([$this, 'onShutdown']);
        pcntl_signal(SIGTERM, [$this, 'onSignal']);
        pcntl_signal(SIGHUP, [$this, 'onSignal']);
        pcntl_signal(SIGINT, [$this, 'onSignal']);

        $workerId = md5(microtime());
        $this->info("worker({$workerId}) running..");

        // Run
        while (1) {
            $queueItem = $this->queue->process($priorityLevel);
            if ($queueItem) {
                $t = date("Y-m-d H:i:s");
                $this->info("[{$t}] Worker({$workerId}) executed QueueItem({$queueItem->_id}): {$queueItem->getTask()
                ->class_name}");
            }
            sleep(self::WORKER_SLEEP_TIME);
        }
    }

    /**
     * Show queue status
     */
    public function actionStatus() {
        $itemsProvider = $this->queue->getActiveQueueItems();
        $this->title("Queue status");
        $this->success("Total active queued tasks: {$itemsProvider->getTotalCount()}");
        $index = 0;
        foreach ($itemsProvider->query->all() as $queueItem) {
            /** @var QueueItem $queueItem */
            $this->info("{$index}: queue-item(". json_encode($queueItem->toArray()). ") | task(" . json_encode($queueItem->getTask()->toArray()) . ")");
            $index++;
        }
    }

    /**
     * Process next task in the queue
     *
     * @param bool|false $priorityLevel
     */
    public function actionProcess($priorityLevel = false) {
        $this->title("Processing... ");
        $queueItem = $this->queue->process($priorityLevel);
        if($queueItem) {
            $this->info("queue-item(". json_encode($queueItem->toArray()). ") | task(" . json_encode($queueItem->getTask()->toArray()) . ")");
        } else {
            $this->info("Nothing to process.");
        }
    }

    /**
     * List available queue tasks
     */
    public function actionListTasks() {
        $this->info("Available task classes:");
        foreach ($this->queue->getDefinedTasks() as $key => $tasks) {
            $this->title("  {$key}");
            foreach ($tasks as $task) {
                $this->success("    {$key}/{$task}");
            }
        }
    }

    /**
     * Add new task to the job queue
     *
     * @param $taskName
     * @param array|null $params
     * @return int
     * @throws \Exception
     */
    public function actionAdd($taskName, array $params = null) {
        $nameParts = explode("/", $taskName);
        if(count($nameParts)!==2) { // check name format, eg. user/PublishProfileImageTask
            throw new \Exception("Invalid task name '{$taskName}'");
        }

        $type = $nameParts[0]; $name = $nameParts[1];
        if(!$this->queue->hasTask($type, $name)) {
            throw new \Exception("No task found by name '{$type}/{$name}'");
        }

        $parsedParams = $this->parseTaskParameters($params);

        // Create new task model
        $task = $this->queue->createTask($type, $name);
        $task->setAttributes($parsedParams);

        if($task->save()) {
            $task->refresh();
            $this->success("Task '{$type}/{$name}' saved, task-id: {$task->_id}");
        } else if(!$task->hasErrors()) {
            throw new \Exception("Cannot create task '{$type}/{$name}', unknown error.");
        }

        if($task->hasErrors()) {
            $this->error("Cannot create new '{$type}/{$name}', validation failed.");
            $this->info(VarDumper::dumpAsString($task->errors));
            return 1;
        }

        $queueItem = $this->queue->add($task);
        if ($queueItem) {
            $this->success("'{$type}/{$name}' added to the queue, queue-item-id: {$queueItem->_id}");
        } else {
            $this->error("Could not queue task.");
            return 1;
        }

        return 0;
    }

    /**
     * Parse CLI parameter format: user=foo name=bar
     *
     * @param array $params
     * @return array|int
     */
    private function parseTaskParameters($params) {
        // Parse passed parameters
        $parsedParams = [];
        if(is_array($params)) {
            $i=0;
            foreach ($params as $paramStr) {
                $assignment = explode("=", $paramStr);
                if(count($assignment)!=2) {
                    $this->stderr("Invalid parameter format at position: {$i}, value: '$paramStr'. (Example: a=123)" . PHP_EOL);
                    return 1;
                }

                list($paramName, $paramValue) = $assignment;
                $parsedParams[$paramName] = $paramValue;
                $i++;
            }
        }

        return $parsedParams;
    }
}