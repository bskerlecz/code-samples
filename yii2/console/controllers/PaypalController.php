<?php

namespace console\controllers;

use common\components\paypal\PaypalService;
use PayPal\Api\WebhookEventType;
use PayPal\Exception\PayPalConnectionException;
use yii\console\Controller;

/**
 * CLI for listing/registering and removing PayPal webhooks
 *
 * Class PaypalController
 * @package console\controllers
 */
class PaypalController extends Controller
{
    /**
     * Lists currently registered PayPal webhooks
     *
     * @return mixed
     */
    public function actionListWebhooks() {
        /** @var PaypalService $paypal */
        $paypal = \Yii::$app->paypal;

        $this->stdout("Getting list of webhooks from PayPal." . PHP_EOL);
        $list = $paypal->getRegisteredWebhooks();
        $webhookCount = count($list->getWebhooks());

        $this->stdout("Found {$webhookCount} webhooks for this application." . PHP_EOL);
        if ($webhookCount > 0) {
            foreach ($list->getWebhooks() as $webhook) {
                $events = [];
                foreach ($webhook->getEventTypes() as $event) {
                    $events[] = $event->name;
                }

                $this->stdout("{$webhook->toJSON()}". PHP_EOL);
            }
        }

        return static::EXIT_CODE_NORMAL;
    }

    /**
     * Adds the PayPal's notification callback to this application instance
     *
     * @return mixed
     */
    public function actionAddNotificationWebhook() {
        /** @var PaypalService $paypal */
        $paypal = \Yii::$app->paypal;

        $this->stdout("Trying to add url: '{$paypal->getWebhookUrl()}' to PayPal webhooks." . PHP_EOL);
        $webhooks = $paypal->getWebhooksForUrl($paypal->getWebhookUrl());
        if (empty($webhooks)) {
            try {
                $webhook = $paypal->addWebhookForUrlAllEventTypes($paypal->getWebhookUrl());
            } catch (\Exception $e) {
                $this->stderr(get_class($e) . ": {$e->getMessage()}, trace: {$e->getTraceAsString()}" . PHP_EOL);
                if ($e instanceof PayPalConnectionException) {
                    $this->stderr($e->getData() . PHP_EOL);
                }
                return static::EXIT_CODE_ERROR;
            }
            $this->stdout("{$webhook->toJSON()}" . PHP_EOL);
        } else {
            $this->stdout("Webhook already exists for url: {$paypal->getWebhookUrl()}, doing nothing." . PHP_EOL);
            return static::EXIT_CODE_ERROR;
        }

        return static::EXIT_CODE_NORMAL;
    }

    /**
     * Remove PayPal's notification callback from this application  instance
     *
     * @throws \Exception
     */
    public function actionRemoveNotificationWebhook() {
        /** @var PaypalService $paypal */
        $paypal = \Yii::$app->paypal;

        $this->stdout("Trying to remove url: '{$paypal->getWebhookUrl()}' from PayPal webhooks." . PHP_EOL);
        $webhooks = $paypal->getWebhooksForUrl($paypal->getWebhookUrl());
        if (empty($webhooks)) {
            $this->stdout("Webhook does not exists for url: {$paypal->getWebhookUrl()}, doing nothing." . PHP_EOL);
        } else {
            // Throw and error if an url has more than one webhooks, need to update it manually since we cant deal with it
            if (count($webhooks) > 1) {
                throw new \Exception("Url: {$paypal->getWebhookUrl()} is registered more than once, doing nothing.");
            }

            $webhook = $webhooks[0];
            $result = $paypal->removeWebhook($webhook);
            $this->stdout("Webhook '{$webhook->toJSON()}' remove " . ($result ? "succeeded" : "failed") . PHP_EOL);
        }
    }
}