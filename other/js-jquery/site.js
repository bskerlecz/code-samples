// Sample site.js

window.console = window.console || { log: function () {} }; // console.log IE polyfill

// Page global configuration
var globalConfig = {
    site_url: 'http://shop.com/',
    cart_url: 'http://shop.com/index.php?action=cart',
    language: 'hu'
};

// Main page script and handlers
var main = (function(cfg) {

    var bodySelector = '#page-content'; // which is the main content holder element on the site
    var scrollInterval = 300; // default scroll interval
    var carouselItemSizes = [[0, 1], [265, 2], [390, 3], [515, 4], [640, 5], [765, 6], [890, 7]]; // Carousel component image sizes

    /**
     * Turn page loader animation on off
     * @param show
     */
    function loader(show) {
        var $loader = $('#xhr-loader');
        if (!$loader.length) return;

        if (show) $loader.show();
        else $loader.hide();
    }

    /**
     * Scrolls to a given element
     * @param hash
     */
    function scrollTo(sel, interval) {
        if(sel === undefined || !$(sel).length) return;
        $('html, body').animate(
            { scrollTop: $(sel).offset().top },
            (interval === undefined ? scrollInterval : interval),
            function() {}
        );
    }

    function appendUrlSeparator(url) {
        if (url.indexOf('?')!==-1) return url + '&';
        else return url + '?';
    }

    /**
     * Dynamically update current page after an xhr call
     * s
     * @param data
     * @param sel
     */
    function updatePage(data, sel) {
        var contentSel = (sel===undefined ? bodySelector : sel);

        $(contentSel).html(data);
        loader(false); // turn off page loader animation
        scrollTo(contentSel);
        bindPageElements(); // bind elements after xhr changes
    }

    /**
     * Bind the search form
     */
    function bindSearchForm() {
        var $search = $('#search_form');
        if (!$search.length) return;

        var $clear          = $('button.clear', $search),
            $filterNone     = $('.filter input.none', $search),
            $filterOption   = $('.filter input:not(.none)', $search),
            $name           = $("input[name='filter_name']", $search),
            $filterCats     = $('.categories-filter', $search),
            $filterAttrs    = $('.attributes-filter', $search);

        function clearFilters() {
            $("input[type='checkbox']", $search).removeAttr('checked');
        }

        // Submit function for the search form
        function submit(e) {
            if (e) e.preventDefault();

            var name = $name.val();
            var orig = $name.attr('data-original-value');
            if (name != orig) clearFilters();

            var url = appendUrlSeparator($search.attr('action')) + $search.serialize();
            $.get(url, null, function(data) { updatePage(data); });

            loader(true); // show animated loader while loading
        }

        // Bind search form and form elements
        $clear.bind('click', function(e) { $name.val(''); });
        $filterAttrs.bind('change', function() { submit(); });
        $filterCats.bind('change', function() { submit(); });
        $search.bind('submit', function(e) { submit(e); })
    }

    // Initializes social share buttons on the page
    function initShareButtons() {
        var $share = $('$share');
        if (!$share.length) return;

        $share.jsSocials({
            showCount: false,
            showLabel: false,
            shares: ['facebook', 'twitter', 'email']
        })
    }

    // initializes the Carousels on the page
    function initCarousel() {
        var $carousel = $('.product-list-carousel');

        if ($carousel.length) {
            $carousel.owlCarousel({
                navigation: false,
                itemsCustom: carouselItemSizes
            });
        }
    }

    // Executes function 'pageScript' if defined
    function executePageScript() {
        if (typeof(pageScript) === 'function') {
            pageScript();
        }
    }

    function bindPageElements() {
        bindSearchForm();
        initCarousel();
        initShareButtons();
    }

    // exported methods
    return {
        init: function() {
            bindPageElements();
            executePageScript();
        },
        scrollTo: function(hash) { scrollTo(hash); },
        loader: function(show) { loader(show); },
    }
})(globalConfig);

// page + jQuery init
(function($) {
    // Initialize page when document loaded
    $(document).ready(function() {
        main.init();
    });
})(jQuery);

